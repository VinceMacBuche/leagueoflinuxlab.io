# Contributing to the League of Linux Wiki

See the [README](README.md) for information on build dependencies.

## Objectives of the Wiki

This wiki is intended to be a catch-all place for information related to playing Riot Games titles on Linux-based operating systems.

To accomplish this, information in the wiki is broadly broken down into three categories:

1. Step-by-step instructions
2. General information and links to external resources
3. Historical or meta information

As much content as possible should be hosted on site to provide the most seemless experience for readers. However, that is not always possible, such as with dependant projects that update frequently, or community discussions. In such cases, external links or admonitions with notices are appropriate.

## Directory Layout

The general site-wide configuration can be found in `mkdocs.yml`, all site content can be found in `docs/`.

Inside the root folder is also `overrides/` which contains override HTML features like custom banners.

Within `docs/` all child folders denote seperate sections within the wiki, except for:

- `assets/` in which you can find all related images, fonts, and other site assets
- `stylesheets/` which contains override CSS for features like custom fonts and admonitions

Within the `docs/` folder, and child category folders, are `index.md` files which are the default landing pages of their respective categories. Specific non-default pages are denoted by individually named files, eg. `lutris.md` and `teamfight_tactics.md`.

## How to Contribute

For small changes, typos, or topics that require further discussion, please [open an issue](https://gitlab.com/leagueoflinux/leagueoflinux.gitlab.io/-/issues).

If you would like to submit directly suggestions or changes, please [submit a merge request](https://gitlab.com/leagueoflinux/leagueoflinux.gitlab.io/-/merge_requests).

Please note that all changes will be subject to review, and may or may not be accepted.

## Markdown Resources

[MkDocs-Material Reference](https://squidfunk.github.io/mkdocs-material/reference/admonitions/)  
[Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)    