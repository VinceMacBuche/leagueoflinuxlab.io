# Installing League of Legends via Other Methods

## Important Information

For the majority of players installing via Lutris is the recommended method. However, as the saying goes, _there's more than one way to skin a ~~cat~~ Rengar_, and in that spirit, there are certainly more than three ways to play League of Legends on Linux.

These methods can be experimental, technically complex, come with heavy drawbacks, or are untested. Use at your own risk!

## Streaming via nvidia's GeForce Now service

[GeForce Now](https://www.nvidia.com/en-us/geforce-now) is nvidia's cloud gaming platform. The platform can installed [via Snap](https://snapcraft.io/geforcenow), `sudo snap install geforcenow`, [or manually](https://github.com/hmlendea/geforcenow-electron). From inside the platform you can select League of Legends to play.

* ⚠️ This method will introduce higher latency to your setup, this can go upwards from 80-100ms in some cases
* ⚠️ This method requires a paid subscription to use regularly as the free tier requires waiting in queues and time limits of one hour per session


## VFIO, GPU passthrough into a Windows virtual machine

This method requires significant technical skills and exact steps vary drastically depending on hardware and software stacks. As such, discussions on r/leagueoflinux about VFIO configurations are welcome, but support requests may be better suited for communities such as r/VFIO or r/linux_gaming.

Riot have confirmed that legitimate virtualisation and GPU-passthrough are acceptable methods of playing, for more read [Vanguard Anticheat? What is it? Is it Coming to League?](../faq/vanguard.md)

## Manually configuring and running a Wine environment

Ultimately, a large part of what the recommended tools accomplish is abstracting away and automating the creation and management of the Wine environment. Nothing is stopping you from entering all the necessary commands manually, should you desire!
