# Wild Rift

* 📱 Available on [Android](https://play.google.com/store/apps/details?id=com.riotgames.league.wildrift) and [iOS](https://apps.apple.com/ph/app/league-of-legends-wild-rift/id1480616990).
* Elsewhere on reddit at r/wildrift.